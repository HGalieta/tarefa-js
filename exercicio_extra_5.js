const booksByCategory = [
  {
    category: "Riqueza",
    books: [
      {
        title: "Os segredos da mente milionária",
        author: "T. Harv Eker",
      },
      {
        title: "O homem mais rico da Babilônia",
        author: "George S. Clason",
      },
      {
        title: "Pai rico, pai pobre",
        author: "Robert T. Kiyosaki e Sharon L. Lechter",
      },
    ],
  },
  {
    category: "Inteligência Emocional",
    books: [
      {
        title: "Você é Insubstituível",
        author: "Augusto Cury",
      },
      {
        title: "Ansiedade – Como enfrentar o mal do século",
        author: "Augusto Cury",
      },
      {
        title: "Os 7 hábitos das pessoas altamente eficazes",
        author: "Stephen R. Covey"
      }
    ]
  }
];

//Contagem do número de categorias:
console.log(`Quantidade de categorias: ${booksByCategory.length}`);
console.log('');

//Contagem do número de livros por categoria:
console.log('Quantidade de livros por categoria');
booksByCategory.forEach(category => {
  console.log(`${category.category}: ${category.books.length}`)
});
console.log('');

//Contagem dos autores:
var autores = [];
booksByCategory.forEach(category => {

  category.books.forEach(book => {

    if (!autores.includes(book.author)) {

      autores.push(book.author);
    };
  })
});

console.log(`Total de autores: ${autores.length}`);
console.log('');

//Listando de livros do autor Augusto Cury:
console.log('Livros do autor Augusto Cury');
booksByCategory.forEach(category => {

  category.books.forEach(book => {

    if (book.author == 'Augusto Cury') {
      console.log(book);
    }
  })
});

//Transformando listagem de livros em função:
function listaLivrosPorAutor(autor) {
  console.log(`Livros do autor ${autor}`);
booksByCategory.forEach(category => {

  category.books.forEach(book => {

    if (book.author == autor) {
      console.log(book);
    }
  })
})
}