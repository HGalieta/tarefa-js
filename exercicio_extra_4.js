let nota = parseInt(Math.random()*100);

function transformaNota(nota) {
  if (nota >= 90) {
    return 'A';
  } else if (nota < 90 && nota >= 80) {
    return 'B';
  } else if (nota < 80 && nota >= 70) {
    return 'C';
  } else if (nota < 70 && nota >= 60) {
    return 'D';
  } else if (nota < 60) {
    return 'F';
  }
}

console.log(nota);
nota = transformaNota(nota);
console.log(nota);