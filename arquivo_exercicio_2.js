// Rodar o arquivo no node com o comando "node ./arquivo_exercicio_2.js"

//Configurando o uso do prompt no node:
var prompt = require('prompt-sync')();

//Perguntando ao usuário a temperatura em °F:
let temperaturaFahrenheit = prompt("Escreva uma temperatura em graus Fahrenheit para convertê-la:");

//Verificando se o valor informado é um número:
if (!isNaN(temperaturaFahrenheit)) {

  //Aplicando o cálculo:
  let temperaturaCelcius = ((temperaturaFahrenheit - 32)*5)/9;
  //Imprimindo a resposta:
  console.log("Esta temperatura em graus Celcius é equivalente a: " + temperaturaCelcius);
} else {

  //Imprimindo resposta de erro:
  console.log("Digite um valor numérico válido para uma temperatura.")
}