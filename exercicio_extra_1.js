const prompt = require('prompt-sync')();

var numeros = [];

pedeNumero();
pedeNumero();
pedeNumero();
pedeNumero();
pedeNumero();
pedeNumero();

function pedeNumero() {

  let numero = prompt('Digite um número positivo ou negativo: ');

  while (numero == 0) {

    numero = prompt('Digite um número diferente de zero, positivo ou negativo: ');
  }
  
  numeros.push(numero);
}

var numerosPositivos = [];
var numerosNegativos = [];

numeros.forEach(numero => {
  if (numero > 0) {

    numerosPositivos.push(numero);
  } else if (numero < 0) {

    numerosNegativos.push(numero);
  }
});

console.log(`Quantidade de números positivos: ${numerosPositivos.length}`);
console.log(`Quantidade de números negativos: ${numerosNegativos.length}`);
