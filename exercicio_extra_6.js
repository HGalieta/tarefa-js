const financas = {
  receitas: [
    3000.00, 150.00, 33.00,
  ],
  despesas: [
    1500.00, 427.00, 55.00, 38.00, 337.00
  ]
}

function calculaSaldo(obj){
  let ganhos = obj.receitas.reduce((total, valor) => {
    return total + valor;
  }, 0);

  let gastos = obj.despesas.reduce((total, valor) => {
    return total + valor;
  }, 0)

  if (ganhos > gastos) {
    console.log(`A família está com saldo positivo de: ${ganhos - gastos}.`);
  } else if (ganhos < gastos) {
    console.log(`A família está com saldo negativo de: ${gastos - ganhos}.`);
  } else if (ganhos == gastos) {
    console.log('A família está com saldo 0.');
  }
}

calculaSaldo(financas);