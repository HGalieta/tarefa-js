//Criando a função:

function multiplicaMatrizes (matriz1, matriz2){
  return [
    [(matriz1[0][0]*matriz2[0][0] + matriz1[0][1]*matriz2[1][0]),  (matriz1[0][0]*matriz2[0][1] + matriz1[0][1]*matriz2[1][1])],
    [(matriz1[1][0]*matriz2[0][0] + matriz1[1][1]*matriz2[1][0]),  (matriz1[1][0]*matriz2[0][1] + matriz1[1][1]*matriz2[1][1])]
  ]
}

//Aplicando aos exemplos:

const exemplo1_1 = [[2, -1], [2, 0]];
const exemplo1_2 = [[2, 3], [-2, 1]];

const exemplo2_1 =  [[4, 0], [-1, -1]];
const exemplo2_2 =  [[-1, 3], [2, 7]];

console.log("Exemplo 1:")
console.log(multiplicaMatrizes(exemplo1_1, exemplo1_2));
console.log("")
console.log("Exemplo 2:")
console.log(multiplicaMatrizes(exemplo2_1, exemplo2_2));
console.log("")

//Aplicando a outras matrizes como exercício:

const exercicio1 = [[3, 5], [2, 4]];
const exercicio2 = [[6, 12], [7, 5]];

console.log("Exercício:")
console.log(multiplicaMatrizes(exercicio1, exercicio2));