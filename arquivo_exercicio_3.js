const prompt = require('prompt-sync')();

const nota1 = prompt("Qual a primeira nota:");

const nota2 = prompt("Qual a segunda nota:");

const nota3 = prompt("Qual a terceira nota:");

const media = (parseFloat(nota1.replace(',', '.')) + parseFloat(nota2.replace(',', '.')) + parseFloat(nota3.replace(',', '.')))/3;

if(media >= 6.0) {

  console.log("Aprovado");
} else {

  console.log("Reprovado");
}