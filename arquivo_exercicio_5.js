let numero1 = parseInt(Math.random()*100);
let numero2 = parseInt(Math.random()*100);
let numero3 = parseInt(Math.random()*100);
let numero4 = parseInt(Math.random()*100);
let numero5 = parseInt(Math.random()*100);

function verificaDivisão(numero) {

  var dividePor3 = numero % 3 == 0;
  var dividePor5 = numero % 5 == 0;

  if (dividePor3 && !dividePor5){

    return "fizz";
  } if (dividePor5 && !dividePor3) {

    return "buzz";
  } if ( dividePor3 && dividePor5) {

    return "fizzbuzz";
  }
  return "";
}

console.log(`Para o número ${numero1}: ${verificaDivisão(numero1)}`);
console.log(`Para o número ${numero2}: ${verificaDivisão(numero2)}`);
console.log(`Para o número ${numero3}: ${verificaDivisão(numero3)}`);
console.log(`Para o número ${numero4}: ${verificaDivisão(numero4)}`);
console.log(`Para o número ${numero5}: ${verificaDivisão(numero5)}`);