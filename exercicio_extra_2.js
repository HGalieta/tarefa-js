var numeros = [];

function multiplica() {
  
  if (numeros.length < 2) {

    let numero = parseInt(Math.random()*100);
    numeros.push(numero);
    console.log(`Número ${numeros.length}: ${numero}`)

    multiplica();
  } 
  
  return numeros[0] * numeros[1]
}

var produto = multiplica();

console.log(`O produto dos números é igual a: ${produto}`);